
# Métodos computacionales para ingeniería 1 - UNComa (2020)

## Datos de la práctica 3

Los datos se obtuvieron del siguiente dataset:

(Real and Fake Face Detection. Computational Intelligence and Photography Lab Department of Computer Science, Yonsei University)[https://www.kaggle.com/ciplab/real-and-fake-face-detection/data]

## Acerca de este repositorio 
Este repositorio contiene las imagenes a las cuales se renombró mediante un script generado por linea de comandos. Las mismas se encuentran en la carpeta dataSet, junto con la información de sus nombres anteriores en los archivos dat.
Las carpetas restantes contienen el script utilizado para el cambio de nombres.